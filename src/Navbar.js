import LanguageSelect from "./languageSelect";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

const Navbar = ({ user }) => {
  const { t } = useTranslation();

  return (
    <div className="nav-bar">
      {user && user.isDoctor === "no" && (
        <ul>
          <li className="nav-bar-item" id="nav-bar-logo">
            <img src={require("/img/dentaWeb.png")} alt="logo"></img>
          </li>
          <li className="nav-bar-item" id="nav-bar-home">
            <Link to="/">{t("Home")}</Link>
          </li>
          <li className="nav-bar-item">
            <LanguageSelect />
          </li>
          <li className="nav-bar-item">
            <Link to="/Noutati">{t("News")}</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Contact">Contact</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Medici">{t("Doctors")}</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/DespreNoi">{t("About Us")}</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/ServiciiTarife">{t("Services")}</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Profile">Profile</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Logout">Logout</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Appointment">Appointment</Link>
          </li>
        </ul>
      )}
      {user && user.isDoctor === "yes" && (
        <ul>
          <li className="nav-bar-item" id="nav-bar-logo">
            <img src={require("/img/dentaWeb.png")} alt="logo"></img>
          </li>
          <li className="nav-bar-item" id="nav-bar-home">
            <Link to="/">{t("Home")}</Link>
          </li>
          <li className="nav-bar-item">
            <LanguageSelect />
          </li>
          <li className="nav-bar-item">
            <Link to="/Profile">Profile</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Logout">Logout</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/DoctorSchedule">Schedule</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/DoctorChart">Chart</Link>
          </li>
        </ul>
      )}
      {!user && (
        <ul>
          <li className="nav-bar-item" id="nav-bar-logo">
            <img src={require("/img/dentaWeb.png")} alt="logo"></img>
          </li>
          <li className="nav-bar-item" id="nav-bar-home">
            <Link to="/">{t("Home")}</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Login">Login</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Register">Register</Link>
          </li>
          <li className="nav-bar-item">
            <LanguageSelect />
          </li>
          <li className="nav-bar-item">
            <Link to="/Noutati">{t("News")}</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Contact">Contact</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/Medici">{t("Doctors")}</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/DespreNoi">{t("About Us")}</Link>
          </li>
          <li className="nav-bar-item">
            <Link to="/ServiciiTarife">{t("Services")}</Link>
          </li>
        </ul>
      )}
    </div>
  );
};

export default Navbar;
