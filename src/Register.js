import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./login.css";

export default function Register() {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [isDoctor, setIsDoctor] = useState("");
  const history = useHistory();

  async function handleSubmit(e) {
    e.preventDefault();
    //const medicalServiceId = "Service1";
    //const isDoctorLowerCase = isDoctor.toLowerCase();
    let user = { name, password, email, isDoctor };
    let result = await fetch("http://localhost:8000/api/register", {
      method: "POST",
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    });
    result = await result.json();
    localStorage.setItem("user-info", JSON.stringify(result));
    window.location = "/";
  }

  return (
    <form className="box" onSubmit={handleSubmit}>
      <h1>Register</h1>
      <input
        type="text"
        name=""
        placeholder="Name"
        onChange={(e) => setName(e.target.value)}
      ></input>
      <input
        type="text"
        name=""
        placeholder="Email"
        onChange={(e) => setEmail(e.target.value)}
      ></input>
      <input
        type="password"
        placeholder="Password"
        onChange={(e) => setPassword(e.target.value)}
      ></input>
      <input
        type="text"
        placeholder="isDoctor(yes/no)"
        onChange={(e) => setIsDoctor(e.target.value)}
      ></input>
      <input type="submit" name="" value="Register"></input>
    </form>
  );
}
