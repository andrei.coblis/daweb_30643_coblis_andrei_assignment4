import react, { useState, useEffect } from "react";
import { Button, Form, FormGroup } from "react-bootstrap";

export default function Profile() {
  const [userId, setUserId] = useState();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [medicalServices, setMedicalServices] = useState("");

  useEffect(() => {
    var userInfo = localStorage.getItem("user-info");
    var test = JSON.parse(userInfo);
    setName(test.name);
    setEmail(test.email);
    setUserId(test.id);
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();
    var medicalServiceId = medicalServices;
    let user = { name, password, email, medicalServiceId };
    let result = await fetch(`http://localhost:8000/api/update/${userId}`, {
      method: "PUT",
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    });
    result = await result.json();
    localStorage.setItem("user-info", JSON.stringify(result));
  }

  return (
    <div>
      <Form onSubmit={handleSubmit}>
        <FormGroup controlId="formName">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
        </FormGroup>
        <FormGroup controlId="formEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="text"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </FormGroup>
        <FormGroup controlId="formPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Enter new password"
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </FormGroup>
        <FormGroup controlId="formMedicalServices">
          <Form.Label>Medical Services</Form.Label>
          <Form.Control
            as="Select"
            multiple
            onChange={(e) => {
              setMedicalServices(e.target.value);
            }}
          >
            <option>Service1</option>
            <option>Service2</option>
            <option>Service3</option>
            <option>Service4</option>
          </Form.Control>
        </FormGroup>
        <FormGroup controlId="formGDPR">
          <Form.Label>Upload GDPR consent</Form.Label>
          <Form.File
            onChange={(e) => {
              console.log(e.target.files);
            }}
          />
        </FormGroup>
        <Button variant="primary" type="submit" onSubmit={handleSubmit}>
          Submit
        </Button>
      </Form>
    </div>
  );
}
