import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

export default function Appointment() {
  const [name, setName] = useState();
  const [userId, setUserId] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [appointments, setAppointments] = useState();

  async function fetchData() {
    await axios.get("http://localhost:8000/api/appointments").then((res) => {
      setAppointments(res.data);
      console.log(appointments);
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();
    const startDateFormatted = moment(startDate).format("YYYY-MM-DDThh:mm:ss");
    const endDateFormatted = moment(endDate).format("YYYY-MM-DDThh:mm:ss");
    await axios.post("http://localhost:8000/api/addappointment", {
      text: name,
      userId: userId,
      startDate: startDateFormatted,
      endDate: endDateFormatted
    });
  }

  return (
    <div>
      <h1>Appointment</h1>
      <Form
        style={{
          paddingLeft: "100px",
          paddingRight: "100px",
          paddingBottom: "30px"
        }}
      >
        <Form.Label>Enter Doctor Id</Form.Label>
        <Form.Control
          onChange={(e) => setUserId(e.target.value)}
        ></Form.Control>
        <Form.Label>Appointment For</Form.Label>
        <Form.Control onChange={(e) => setName(e.target.value)}></Form.Control>
        <Form.Label>Start Date</Form.Label>
        <Form.Control
          onChange={(e) => setStartDate(e.target.value)}
        ></Form.Control>
        <Form.Label>End Date</Form.Label>
        <Form.Control
          onChange={(e) => setEndDate(e.target.value)}
        ></Form.Control>
      </Form>
      <Button onClick={handleSubmit} style={{ margin: "30px" }}>
        Submit Appointment
      </Button>
    </div>
  );
}
