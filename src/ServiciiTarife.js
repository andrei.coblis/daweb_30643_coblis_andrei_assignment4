import { Container, Col, Carousel } from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { useTranslation } from "react-i18next";
export default function ServiciiTarife() {
  const { t } = useTranslation();
  return (
    <div>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>
        {t("Services")}
      </h1>
      <div>
        <Container fluid>
          <Col className="carousel-content">
            <Carousel>
              <Carousel.Item>
                <img
                  style={{ height: "400px", width: "100vh" }}
                  src={require("/img/Pediatric dentistry.jpg")}
                  alt="First slide"
                />
                <Carousel.Caption>
                  <h3 style={{ fontFamily: "Comfortaa", color: "black" }}>
                    {t("Servicii_text_1")}
                  </h3>
                  <Link to="/contact" className="btn btn-primary">
                    Contact
                  </Link>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  style={{ height: "400px", width: "100vh" }}
                  src={require("/img/General care.jpg")}
                  alt="Second slide"
                />

                <Carousel.Caption>
                  <h3 style={{ fontFamily: "Comfortaa", color: "black" }}>
                    {t("Servicii_text_2")}
                  </h3>
                  <Link to="/contact" className="btn btn-primary">
                    Contact
                  </Link>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  style={{ height: "400px", width: "100vh" }}
                  src={require("/img/Additional treatment.jpg")}
                  alt="Third slide"
                />

                <Carousel.Caption>
                  <h3 style={{ fontFamily: "Comfortaa", color: "black" }}>
                    {t("Servicii_text_3")}
                  </h3>
                  <Link to="/contact" className="btn btn-primary">
                    Contact
                  </Link>
                </Carousel.Caption>
              </Carousel.Item>
            </Carousel>
          </Col>
        </Container>
      </div>
    </div>
  );
}
