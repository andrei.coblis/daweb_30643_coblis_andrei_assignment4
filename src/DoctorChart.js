import "./chart.css";

var React = require("react");
var Component = React.Component;
var CanvasJSReact = require("./canvasjs.react").default;
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var dataPoints = [];
const doctor = JSON.parse(localStorage.getItem("user-info"));

class DoctorChart extends Component {
  componentDidMount() {
    var chart = this.chart;
    fetch(`http://localhost:8000/api/appointments/${doctor.id}`)
      .then(function (response) {
        return response.json();
      })
      .then(function (data) {
        var dict = {};
        console.log(data);
        for (var j = 0; j < data.length; j++) {
          if (dict[data[j].startDate.toString().substring(0, 10)] === 1)
            dict[data[j].startDate.toString().substring(0, 10)]++;
          else dict[data[j].startDate.toString().substring(0, 10)] = 1;
        }
        for (var key in dict) {
          var value = dict[key];
          dataPoints.push({
            x: new Date(key),
            y: value
          });
        }
        dataPoints.push({
          x: new Date("2021-05-12"),
          y: 2
        });
        dataPoints.push({
          x: new Date("2021-05-15"),
          y: 5
        });
        dataPoints.push({
          x: new Date("2021-05-18"),
          y: 1
        });
        chart.render();
      });
  }

  render() {
    const options = {
      animationEnabled: true,
      exportEnabled: true,
      theme: "light2", //"light1", "dark1", "dark2"
      title: {
        text: "Simple Column Chart with Index Labels"
      },
      axisY: {
        includeZero: true
      },
      data: [
        {
          type: "column", //change type to bar, line, area, pie, etc
          //indexLabel: "{y}", //Shows y value on all Data Points
          indexLabelFontColor: "#5A5757",
          indexLabelPlacement: "outside",
          valueFormatString: "DD MMM YY",
          dataPoints: dataPoints
        }
      ]
    };

    return (
      <div>
        <CanvasJSChart options={options} onRef={(ref) => (this.chart = ref)} />
        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
      </div>
    );
  }
}
module.exports = DoctorChart;
