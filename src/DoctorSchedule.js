import React from "react";

import Scheduler from "devextreme-react/scheduler";
import axios from "axios";

const currentDate = new Date();
const doctor = JSON.parse(localStorage.getItem("user-info"));
const views = ["day", "week", "workWeek", "month"];

export default class DoctorSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: []
    };
  }

  componentDidMount() {
    axios
      .get(`http://localhost:8000/api/appointments/${doctor.id}`)
      .then((res) => {
        this.setState({ appointments: res.data });
      });
  }
  render() {
    const { appointments } = this.state;
    var data = [];
    if (appointments) {
      for (var i = 0; i < appointments.length; i++) {
        data.push({
          text: appointments[i].text,
          startDate: appointments[i].startDate,
          endDate: appointments[i].endDate
        });
      }
    }
    return (
      <div>
        {data.length > 0 ? (
          <Scheduler
            dataSource={data}
            views={views}
            defaultCurrentView="day"
            defaultCurrentDate={currentDate}
            height={600}
            startDayHour={9}
          />
        ) : (
          <div>No appointments</div>
        )}
      </div>
    );
  }
}
