import "./styles.css";
import "bootstrap/dist/css/bootstrap.min.css";
import React, { Suspense, useEffect, useState } from "react";
import Navbar from "./Navbar";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import Login from "./Login";
import Register from "./Register";
import Profile from "./Profile";
import Logout from "./Logout";
import Appointment from "./Appointment";
import DoctorSchedule from "./DoctorSchedule";
import DoctorChart from "./DoctorChart";
import Home from "./Home";
import Contact from "./Contact";
import DespreNoi from "./DespreNoi";
import Medici from "./Medici";
import Noutati from "./Noutati";
import ServiciiTarife from "./ServiciiTarife";

export default function App() {
  const [userInfo, setUserInfo] = useState("");

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("user-info"));
    setUserInfo(user);

    console.log(userInfo);
  }, []);

  return (
    <Router>
      <div className="App">
        <Suspense fallback="loading">
          <header>
            <Navbar user={userInfo} />
          </header>
          {userInfo && userInfo.isDoctor === "no" && (
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/Noutati">
                <Noutati />
              </Route>
              <Route exact path="/Contact">
                <Contact />
              </Route>
              <Route exact path="/DespreNoi">
                <DespreNoi />
              </Route>
              <Route exact path="/Medici">
                <Medici />
              </Route>
              <Route exact path="/ServiciiTarife">
                <ServiciiTarife />
              </Route>
              <Route exact path="/Login">
                <Login />
              </Route>
              <Route exact path="/Register">
                <Register />
              </Route>
              <Route exact path="/Profile">
                <Profile />
              </Route>
              <Route exact path="/Logout">
                <Logout />
              </Route>
              <Route exact path="/Appointment">
                <Appointment />
              </Route>
            </Switch>
          )}
          {userInfo && userInfo.isDoctor === "yes" && (
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/Noutati">
                <Noutati />
              </Route>
              <Route exact path="/Contact">
                <Contact />
              </Route>
              <Route exact path="/DespreNoi">
                <DespreNoi />
              </Route>
              <Route exact path="/Medici">
                <Medici />
              </Route>
              <Route exact path="/ServiciiTarife">
                <ServiciiTarife />
              </Route>
              <Route exact path="/Login">
                <Login />
              </Route>
              <Route exact path="/Register">
                <Register />
              </Route>
              <Route exact path="/Profile">
                <Profile />
              </Route>
              <Route exact path="/Logout">
                <Logout />
              </Route>
              <Route exact path="/Appointment">
                <Appointment />
              </Route>
              <Route exact path="/DoctorSchedule">
                <DoctorSchedule />
              </Route>
              <Route exact path="/DoctorChart">
                <DoctorChart />
              </Route>
            </Switch>
          )}
          {!userInfo && (
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/Noutati">
                <Noutati />
              </Route>
              <Route exact path="/Contact">
                <Contact />
              </Route>
              <Route exact path="/DespreNoi">
                <DespreNoi />
              </Route>
              <Route exact path="/Medici">
                <Medici />
              </Route>
              <Route exact path="/ServiciiTarife">
                <ServiciiTarife />
              </Route>
              <Route exact path="/Login">
                <Login />
              </Route>
              <Route exact path="/Register">
                <Register />
              </Route>
            </Switch>
          )}
        </Suspense>
      </div>
    </Router>
  );
}
