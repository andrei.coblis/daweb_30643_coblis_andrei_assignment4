<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;

class AppointmentController extends Controller
{
    //
    function addAppointment(Request $req)
    {
        $appointment = new Appointment;
        $appointment->text = $req->input('text');
        $appointment->userId = $req->input('userId');
        $appointment->startDate = $req->input('startDate');
        $appointment->endDate = $req->input('endDate');
        $appointment->save();

        return $appointment;
    }

    function getAppointments()
    {
        return Appointment::all();
    }

    function getAppointmentsByUserId($userId)
    {
        $appointments = Appointment::where('userId', $userId)->get();
        return $appointments;
    }
}
